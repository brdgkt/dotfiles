# basic aliases that should be present on ANY machine
# if you want to add aliases which are not shared with other machines, add them to $HOME/.config/bash/aliases

alias ll='ls -la'
alias co='cd /opt'
alias ce='cd /etc'
alias ct='cd /tmp'
alias desk='cd $HOME/Desktop'
alias ussh='ssh -o StrictHostKeyChecking=no'
alias dotfiles='git --git-dir=$HOME/.dotfiles --work-tree=$HOME'

