# basic .bash_profile file with fundamental settings that should be present on any machine
# for more specific things, put them into a separate $HOME/.config/bash/profile file
# basic aliases are defined in .bash_aliases
# machine specific aliases can be added by putting them into a $HOME/.config/bash/aliases file


export LANG=en_US.UTF-8


[ -d $HOME/bin ] && export PATH=$PATH:$HOME/bin


# stop .DS_STORE files appearing in tar archives (Mac OS)
export COPYFILE_DISABLE=true


[ $(which nvim) ] && export EDITOR=$(which nvim)


# source machine specific profile file 
[ -f $HOME/.config/bash/profile ] && . $HOME/.config/bash/profile


# source basic aliases file
[ -f $HOME/.bash_aliases ]; . $HOME/.bash_aliases


# source machine specific aliases file
[ -f $HOME/.config/bash/aliases ] && . $HOME/.config/bash/aliases

