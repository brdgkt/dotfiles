set nu
set cursorline
set autoindent
set expandtab
set shiftwidth=2
set softtabstop=2
set showmatch
set foldmethod=indent
set foldlevel=99
set nobackup
set nowritebackup
set encoding=UTF-8
" fixes strange characters appearing on Ubuntu
set guicursor=

" Plugins, see vim-plug https://github.com/junegunn/vim-plug

if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $HOME/.config/nvim/init.vim
endif

call plug#begin('~/.local/share/nvim/plugged')

" Plugin outside ~/.vim/plugged with post-update hook
" Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" ---------- look ----------
" Add color theme
Plug 'junegunn/seoul256.vim'
" Add icons
Plug 'ryanoasis/vim-devicons'
" Vertical indentation lines
Plug 'Yggdroot/indentLine'
" Status bar
Plug 'itchyny/lightline.vim'
" ---------- navigation ----------
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
" ---------- programming ----------
" Linting
Plug 'dense-analysis/ale'
" ---------- misc ----------
" Fuzzy search
Plug 'junegunn/fzf', { 'dir': '~/.local/share/nvim/fzf', 'do': './install --bin' }
Plug 'junegunn/fzf.vim'
" Initialize plugin system
call plug#end()



" dense-analysis/ale
let g:ale_sign_error = '✖'
let g:ale_sign_warning = '⚠'
" hi todo ctermbg=2 ctermfg=0
" hi error ctermbg=1 ctermfg=0

" Yggdroot/indentLine
let g:indentLine_char = "\ue621"
let g:indentLine_setConceal = 0

" itchyny/lightline
let g:lightline = {
      \ 'colorscheme': 'seoul256',
      \ }

" Color schemes
"
" seoul256 (dark):
"   Range:   233 (darkest) ~ 239 (lightest)
"   Default: 237
let g:seoul256_background = 234
colo seoul256

" seoul256 (light):
"   Range:   252 (darkest) ~ 256 (lightest)
"   Default: 253
" let g:seoul256_background = 253
" colo seoul256

" junegunn/fzf
let g:fzf_colors =
	\ { 'hl': ['fg', 'Exception'],
        \   'hl+': ['fg', 'Exception'], }



" Custom key mappings
"
"remap leader
let mapleader = ","
" NERDTree
nnoremap <C-n> :NERDTreeToggle<CR>
" FZF search files in current directory
nnoremap <leader>o :FZF<CR>
nnoremap <leader>f :BLines<CR>
nnoremap <leader>F :Rg<CR>


