config.bind('I', 'set-cmd-text -s :open -p')
config.bind(',v', ':hint links spawn mpv {hint-url}')
c.downloads.location.directory = '/tmp/'
c.downloads.location.prompt = False
c.tabs.background = True

